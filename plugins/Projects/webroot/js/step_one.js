$(document).ready(function(){
    $('.search').click(function(e){
        e.preventDefault();
        var rbd = $('#SchoolRBD').val();
        $.ajax({
            url: '/schools/findByRbd/'+rbd,
            success: function(response){
                var json_obj = JSON.parse(response);
                $.each(json_obj,function(key,value){
                    switch(key){
                        case 'id':
                            $('#SchoolId').val(value);
                            break;
                        case 'region':
                        case 'province':
                        case 'district':
                            $('td#td-'+key).text(value.name);
                            break;
                        default:
                            $('td#td-'+key).text(value);
                            break;
                    }
                });
            }
        });
    });
    
    $('.btnGuardar').on('click',function(e){
        e.preventDefault();
        $('#StepOneForm').submit();
    });
});