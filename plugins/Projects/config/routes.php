<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::plugin(
    'Projects',
    ['path' => '/projects'],
    function (RouteBuilder $routes) {
        $routes->fallbacks('DashedRoute');
    }
);
