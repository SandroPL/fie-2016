<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Project'), ['action' => 'edit', $project->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Project'), ['action' => 'delete', $project->id], ['confirm' => __('Are you sure you want to delete # {0}?', $project->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Projects'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="projects view large-9 medium-8 columns content">
    <h3><?= h($project->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($project->id) ?></td>
        </tr>
        <tr>
            <th><?= __('User Id') ?></th>
            <td><?= $this->Number->format($project->user_id) ?></td>
        </tr>
        <tr>
            <th><?= __('School Id') ?></th>
            <td><?= $this->Number->format($project->school_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Step') ?></th>
            <td><?= $this->Number->format($project->step) ?></td>
        </tr>
        <tr>
            <th><?= __('Sended') ?></th>
            <td><?= $this->Number->format($project->sended) ?></td>
        </tr>
        <tr>
            <th><?= __('Stage') ?></th>
            <td><?= $this->Number->format($project->stage) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $this->Number->format($project->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($project->created) ?></td>
        </tr>
    </table>
</div>
