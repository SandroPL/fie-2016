<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?=$this->Flash->render();?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?=__('Proyectos en Proceso');?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <style type="text/css">
                                thead th { color: #3c8dbc; }
                            </style>
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id','ID') ?></th>
                                        <th><?= $this->Paginator->sort('user_id','Postulante') ?></th>
                                        <th><?= $this->Paginator->sort('school_id','Escuela') ?></th>
                                        <th><?= $this->Paginator->sort('step','Paso') ?></th>
                                        <th><?= $this->Paginator->sort('created','Creación') ?></th>
                                        <th class="actions"><?= __('Acciones') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($projects as $project): ?>
                                    <tr>
                                        <td><?= $this->Number->format($project->id) ?></td>
                                        <td><?= $project->user->userinfo->name.' '.$project->user->userinfo->lastname; ?></td>
                                        <td><?= $project->school->name ?></td>
                                        <td><?= $this->Number->format($project->step) ?></td>
                                        <td><?= $project->created->format('d-m-Y H:i') ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link('', ['action' => 'adminView', $project->id]) ?>
                                            <?= $this->Form->postLink('<i class="fa fa-trash"></i>', ['action' => 'adminDelete', $project->id],[
                                                    'confirm' => __('Estás seguro(a) de eliminar postulación ID: {0}?', $project->id),
                                                    'escape' => false,
                                                    'title' => __('Eliminar Postulación')])
                                            ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>