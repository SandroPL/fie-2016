<?=$this->Html->script('Projects.step_one');?>
<h2 class="nomargin"><?=__('Postulaciones');?></h2>
    <b class="sub-title"><?=__('Nueva');?></b>
    <div class="new-step noborder">
        <h4>
            <span class="step-mark">
                <b>1</b>/5
            </span> <?=__('Datos de la Institución');?>
        </h4>
        <div class="searchRbd">
            <?=$this->Form->create($project,['id' => 'StepOneForm']);?>
                <?=$this->Form->hidden('school_id',['id' => 'SchoolId']);?>
                <div class="title-rbd">
                    <b>RBD:</b>
                    <?=$this->Form->input('rbd',[
                        # seteamos el template por default de este input para que no ponga un <div> padre antes del <input>
                        'templates' => [
                            'inputContainer' => '{{content}}'
                        ],
                        'id' => 'SchoolRBD',
                        'label' => false
                    ]);?>
                    <?=$this->Html->link('','#',['class' => 'search', 'title' => __('Buscar')]);?>
                </div>
                <table class="table-list bording">
                    <tr>
                        <td>- <?=__('Nombre');?></td>
                        <td class="b-font" id="td-name"></td>
                    </tr>
                    <tr>
                        <td>- <?=__('Dependencia');?></td>
                        <td class="b-font" id="td-dependence"></td>
                    </tr>
                    <tr>
                        <td>- <?=__('Área');?></td>
                        <td class="b-font" id="td-geographical_area"></td>
                    </tr>
                    <tr>
                        <td>- <?=__('Región');?></td>
                        <td class="b-font" id="td-region"></td>
                    </tr>
                    <tr>
                        <td>- <?=__('Provincia');?></td>
                        <td class="b-font" id="td-province"></td>
                    </tr>
                    <tr class="noborder">
                        <td>- <?=__('Comuna');?></td>
                        <td class="b-font" id="td-district"></td>
                    </tr>
                </table>
                <?=$this->Form->hidden('saveDraftCopy',['value' => 1]);?>
            <?=$this->Form->end();?>
        </div>
    </div>
    <div class="new-step sub txt-center">
        <div class="marginBot">
            <?=$this->Html->link('Cancelar',['plugin' => 'Projects', 'controller' => 'Projects', 'action' => 'index'],['class' => 'button', 'title' => 'Cancelar']);?>
            <?=$this->Html->link('Guardar','#',['class' => 'button green btnGuardar', 'title' => 'Guardar']);?>
        </div>
        <div>
            <a title="Ir a" class="back margin" href="#"><span class="step-mark last-mark"><b>2</b>/5</span> ></a>
        </div>
    </div>