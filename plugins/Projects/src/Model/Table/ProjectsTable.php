<?php
namespace Projects\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Projects Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Schools
 * @property \Cake\ORM\Association\HasMany $Antecedents
 * @property \Cake\ORM\Association\HasMany $ProjectFiles
 * @property \Cake\ORM\Association\HasMany $ProjectInfos
 * @property \Cake\ORM\Association\HasMany $RequestProjects
 *
 * @method \Projects\Model\Entity\Project get($primaryKey, $options = [])
 * @method \Projects\Model\Entity\Project newEntity($data = null, array $options = [])
 * @method \Projects\Model\Entity\Project[] newEntities(array $data, array $options = [])
 * @method \Projects\Model\Entity\Project|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Projects\Model\Entity\Project patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Projects\Model\Entity\Project[] patchEntities($entities, array $data, array $options = [])
 * @method \Projects\Model\Entity\Project findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProjectsTable extends Table{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config){
        parent::initialize($config);

        $this->table('projects');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'Users.Users'
        ]);
        $this->belongsTo('Schools', [
            'foreignKey' => 'school_id',
            'joinType' => 'INNER',
            'className' => 'Schools'
        ]);
        $this->hasMany('Antecedents', [
            'foreignKey' => 'project_id',
            'className' => 'Projects.Antecedents'
        ]);
        $this->hasMany('ProjectFiles', [
            'foreignKey' => 'project_id',
            'className' => 'Projects.ProjectFiles'
        ]);
        $this->hasMany('ProjectInfos', [
            'foreignKey' => 'project_id',
            'className' => 'Projects.ProjectInfos'
        ]);
        $this->hasMany('RequestProjects', [
            'foreignKey' => 'project_id',
            'className' => 'Projects.RequestProjects'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->notEmpty('id', 'create');

        $validator
            ->integer('step')
            ->requirePresence('step', 'create')
            ->notEmpty('step');

        $validator
            ->integer('sended')
            ->requirePresence('sended', 'create')
            ->notEmpty('sended');

        $validator
            ->integer('stage')
            ->requirePresence('stage', 'create')
            ->notEmpty('stage');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules){
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['school_id'], 'Schools'));

        return $rules;
    }
    
    public $_status = [
		1 =>'Ingresado',
		2 =>'Se solicita información adicional al postulante',
		3 =>'Aprobada Solicitud',
		4 =>'Rechazado',
		//5=>'No hay recursos',
		7 =>'Modificado por el postulante',
		100 => 'En proceso'
    ];
    
	public $_stages = [
		1 => 'En revisión',
		2 => 'Evaluando factibilidad',
		3 => 'Evaluando presupuesto',
		4 => 'Aprobación final',
		5 => 'Aprobado',
        6 => 'Rechazado',
		100 => 'En proceso'
    ];
}
