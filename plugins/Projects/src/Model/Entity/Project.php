<?php
namespace Projects\Model\Entity;

use Cake\ORM\Entity;

/**
 * Project Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $school_id
 * @property int $step
 * @property int $sended
 * @property int $stage
 * @property int $status
 * @property \Cake\I18n\Time $created
 *
 * @property \Projects\Model\Entity\User $user
 * @property \Projects\Model\Entity\School $school
 * @property \Projects\Model\Entity\Antecedent[] $antecedents
 * @property \Projects\Model\Entity\ProjectFile[] $project_files
 * @property \Projects\Model\Entity\ProjectInfo[] $project_infos
 * @property \Projects\Model\Entity\RequestProject[] $request_projects
 */
class Project extends Entity{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}