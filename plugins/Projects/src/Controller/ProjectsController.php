<?php
namespace Projects\Controller;

use Projects\Controller\AppController;

/**
 * Projects Controller
 *
 * @property \Projects\Model\Table\ProjectsTable $Projects
 */
class ProjectsController extends AppController{

    /**
     * AdminIndex method
     *
     * @return \Cake\Network\Response|null
     */
    public function adminIndex($view = null){
        $conditions = [];
        switch($view){
            case 'in-progress':
                $conditions['status'] = 100;
                break;
            case 'sended':
                $conditions['Projects.sended'] = 1;
                $conditions['Projects.stage'] = 1;
                $conditions['Projects.status IN'] = [1,2,7];
                break;
            case 'approved':
                $conditions['Projects.stage'] = 5;
                $conditions['Projects.status <>'] = 4;
                break;
            case 'rejected':
                $conditions['Projects.status'] = 4;
                break;
            default:
                
                break;
        }
        $this->paginate = [
            'conditions' => $conditions,
            'contain' => ['Users' => ['Userinfos'],'Schools']
        ];
        $projects = $this->paginate($this->Projects);
        
        $this->set('status',$this->Projects->_status);
        $this->set('stages',$this->Projects->_stages);
        $this->set(compact('projects'));
        $this->set('_serialize', ['projects']);
    }

    /**
     * View method
     *
     * @param string|null $id Project id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null){
        $project = $this->Projects->get($id, [
            'contain' => []
        ]);

        $this->set('project', $project);
        $this->set('_serialize', ['project']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add(){
        $project = $this->Projects->newEntity();
        if ($this->request->is('post')) {
            $project = $this->Projects->patchEntity($project, $this->request->data);
            if ($this->Projects->save($project)) {
                $this->Flash->success(__('The project has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The project could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('project'));
        $this->set('_serialize', ['project']);
    }
    
    public function index(){
        
    }
    
    public function stepOne(){
        $project = $this->Projects->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $draftCopy = $this->request->data['saveDraftCopy'];
            unset($this->request->data['saveDraftCopy']);
            unset($this->request->data['rbd']); # no usaremos esto xD
            
            $this->request->data['sended'] = 0; # No enviado
            $this->request->data['user_id'] = $this->Auth->user('id'); # El usuario logueado
            $this->request->data['status'] = 100; # En Proceso
            $this->request->data['stage'] = 100; # En proceso
            $this->request->data['step'] = $draftCopy ? 1 : 2; # Si solo lo guarda lo dejamos en el paso actual, sino lo dejamos en el paso siguiente
            
            $project = $this->Projects->patchEntity($project, $this->request->data);
            if($this->Projects->save($project)){
                # Si sólo guarda el borrador lo redireccionamos al index sino al paso 2
                $this->redirect(['action' => $draftCopy ? 'index' : 'stepTwo']);
            }
        }
        $this->set(compact('project'));
        $this->set('_serialize', ['project']);
    }
    
    public function stepTwo(){
        
    }
    
    public function stepThree(){
        
    }
    
    public function stepFour(){
        
    }
    
    public function stepFive(){
        
    }

    /**
     * Edit method
     *
     * @param string|null $id Project id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null){
        $project = $this->Projects->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $project = $this->Projects->patchEntity($project, $this->request->data);
            if ($this->Projects->save($project)) {
                $this->Flash->success(__('The project has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The project could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('project'));
        $this->set('_serialize', ['project']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Project id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $project = $this->Projects->get($id);
        if ($this->Projects->delete($project)) {
            $this->Flash->success(__('The project has been deleted.'));
        } else {
            $this->Flash->error(__('The project could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
