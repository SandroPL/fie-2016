<?php
namespace Users\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Users\Model\Table\UserInfosTable;

/**
 * Users\Model\Table\UserInfosTable Test Case
 */
class UserInfosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Users\Model\Table\UserInfosTable
     */
    public $UserInfos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.users.user_infos',
        'plugin.users.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserInfos') ? [] : ['className' => 'Users\Model\Table\UserInfosTable'];
        $this->UserInfos = TableRegistry::get('UserInfos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserInfos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
