<div class="login-box-body">
    <?= $this->Flash->render();?>
    <p class="login-box-msg">
        <?=__('Por favor, introduzca el correo electrónico que utilizó para su registro,
              donde se le enviarán instrucciones para finalizar el proceso de
              recuperación de contraseña.');
        ?>
    </p>
    <?php
        echo $this->Form->create('',[
            'novalidate' => false,
            'templates' => [
                'inputContainer' => '
                <div class="form-group has-feedback">
                    {{content}}
                    <span class="glyphicon {{icoClass}} form-control-feedback"></span>
                </div>'
            ]
        ]);
        
        echo $this->Form->input('email',[
                'placeholder' => __('Email'),
                'label' => false,
                'class' => 'form-control',
                'templateVars' => [
                    'icoClass' => 'glyphicon-envelope'
                ]
        ]);
    ?>
        <div class="row">
            <div class="col-xs-12">
                <?=$this->Form->submit('Enviar',[
                        'class' => 'btn btn-primary btn-block btn-flat pull-right',
                        'style' => 'width:40%;',
                        'div' => false
                ]);?>
            </div>
        </div>
        <div class="row" style="margin: 0;">
            <?=$this->Html->link(__('< Volver'),'/admin/login');?><br />
        </div>
    <?=$this->Form->end();?>
</div>