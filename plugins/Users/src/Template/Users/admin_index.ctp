<?php $this->assign('title', 'FIE | Usuarios'); ?>
<section class="content-header">
    <h1 class="page-header"><?=__('Usuarios');?></h2>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),['action' => 'adminDashboard']);?>
        </li>
        <li class="active">
            <i class="fa fa-users"></i> <?=__('Usuarios');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?=$this->Flash->render();?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?=__('Listado Usuarios Registrados');?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <style type="text/css">
                                thead th { color: #3c8dbc; }
                            </style>
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <th><?= $this->Paginator->sort('id',__('ID')) ?></th>
                                    <th><?= $this->Paginator->sort('userinfo.rut',__('Rut')) ?></th>
                                    <th><?= $this->Paginator->sort('username',__('Username')) ?></th>
                                    <th><?= $this->Paginator->sort('email',__('Email')) ?></th>
                                    <th><?= __('Nombre Completo') ?></th>
                                    <th><?= __('Rol')?></th>
                                    <th><?= $this->Paginator->sort('active',__('Estado')) ?></th>
                                    <th class="actions text-center"><?= __('Acciones') ?></th>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $user): ?>
                                    <tr>
                                        <td><?= $this->Number->format($user->id) ?></td>
                                        <td><?= $user->userinfo->rut ?></td>
                                        <td><?= $user->username ?></td>
                                        <td><?= $user->email ?></td>
                                        <td><?= $user->userinfo->full_name ?></td>
                                        <td><?= $roles[$user->is_admin]; ?></td>
                                        <td><?= $user->active == 1 ? 'Habilitado' : 'Deshabilitado' ?></td>
                                        <td class="actions" style="width: 10%;">
                                            <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                                        ['action' => 'adminEdit', $user->id],
                                                        ['escape' => false,'title' => 'Editar']);
                                            ?>
                                            <?= $this->Html->link('<i class="fa fa-refresh" aria-hidden="true"></i>',
                                                        ['action' => 'adminChangeStatus', $user->id],
                                                        ['escape' => false,'title' => 'Cambiar estado']);
                                            ?>
                                            <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',
                                                        ['action' => 'adminDelete', $user->id],
                                                        ['escape' => false, 'title' => 'Eliminar',
                                                         'class' => 'pull-right',
                                                         'confirm' => __('¿Estás seguro de querer eliminar al usuario ID # {0}?', $user->id)]);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>