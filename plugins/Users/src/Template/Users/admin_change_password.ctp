<div class="login-box-body">
    <?= $this->Flash->render();?>
    <p class="login-box-msg"><?=__('Cambiar contraseña.');?></p>
    <?php
        echo $this->Form->create($user,[
            'novalidate' => true,
            'templates' => [
                'inputContainer' => '
                <div class="form-group has-feedback {{errorClass}}">
                    {{content}}
                    <span class="glyphicon {{icoClass}} form-control-feedback"></span>
                    {{errorMessage}}
                </div>'
            ]
        ]);
        
        echo $this->Form->input('password',[
                'placeholder' => __('Nueva Contraseña'),
                'label' => false,
                'class' => 'form-control',
                'div' => false,
                'templateVars' => [
                    'icoClass' => 'glyphicon-lock',
                    'errorClass' => ($this->Form->isFieldError('password') ? 'has-error' : ''),
                    'errorMessage' => $this->Form->error('password')
                ],
                'error' => false
        ]);
        
        echo $this->Form->input('confirm_password',[
                'placeholder' => __('Confirmar Contraseña'),
                'label' => false,
                'div' => false,
                'type' => 'password',
                'class' => 'form-control',
                'templateVars' => [
                    'icoClass' => 'glyphicon-lock'
                ],
                'error' => false
        ]);
    ?>
        <div class="row">
            <div class="col-xs-12">
                <?=$this->Form->submit('Guardar',[
                        'class' => 'btn btn-primary btn-block btn-flat pull-right',
                        'style' => 'width:40%;',
                        'div' => false
                ]);?>
            </div>
        </div>
    <?=$this->Form->end();?>
</div>