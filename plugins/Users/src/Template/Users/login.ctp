<div class="login-box-body">
    <?= $this->Flash->render();?>
    <p class="login-box-msg"><?=__('Logueate para iniciar sesión.');?></p>
    <?php
        echo $this->Form->create('',[
            'novalidate' => false,
            'templates' => [
                'inputContainer' => '
                <div class="form-group has-feedback">
                    {{content}}
                    <span class="glyphicon {{icoClass}} form-control-feedback"></span>
                </div>'
            ]
        ]);
        
        echo $this->Form->input('username',[
                'placeholder' => __('Nickname'),
                'label' => false,
                'class' => 'form-control',
                'templateVars' => [
                    'icoClass' => 'glyphicon-user'
                ]
        ]);
        
        echo $this->Form->input('password',[
                'placeholder' => __('Contraseña'),
                'label' => false,
                'class' => 'form-control',
                'templateVars' => [
                    'icoClass' => 'glyphicon-lock'
                ]
        ]);
    ?>
        <div class="row">
            <div class="col-xs-12">
                <?=$this->Form->submit('Login',[
                        'class' => 'btn btn-primary btn-block btn-flat pull-right',
                        'style' => 'width:40%;',
                        'div' => false
                ]);?>
            </div>
        </div>
        <div class="row" style="margin: 0;">
            <?=$this->Html->link(__('¿Olvidaste tu contraseña?'),'/admin/recuperar-contrasena');?><br />
            <?#=$this->Html->link(__('Registrarse'),__('/registro'),['class' => 'text-center']);?>
        </div>
    <?=$this->Form->end();?>
</div>