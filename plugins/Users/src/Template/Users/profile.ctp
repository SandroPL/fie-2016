<? if(!isset($edit)): ?>
    <?=__('Mis datos');?><br />
    <?=__('Nombre:');?><?=$user->userinfo->full_name;?><br />
    <?=__('Dirección:');?><?=$user->userinfo->address;?><br />
    <?=__('Teléfono:');?><?=$user->userinfo->tel_cellphone;?><br />
    <?=__('email:');?><?=$user->email;?><br />
    <?=$this->Html->link(__('Editar'),['controller' => 'Users', 'action' => 'profile', 'edit' => true]);?>
<? else: ?>

<?=$this->Form->create($user);?>
    <?=$this->Form->hidden('id');?>
    <?=$this->Form->input('username');?>
    <?=$this->Form->input('password');?>
    <?=$this->Form->input('confirm_password',['type' => 'password']);?>
    <?=$this->Form->input('email');?>
    <?=$this->Form->input('userinfo.rut');?>
    <?=$this->Form->input('userinfo.name');?>
    <?=$this->Form->input('userinfo.lastname');?>
    <?=$this->Form->input('userinfo.address');?>
    <?=$this->Form->input('userinfo.tel_phone');?>
    <?=$this->Form->input('userinfo.tel_cellphone');?>
    <?=$this->Form->submit('Guardar');?>
<?=$this->Form->end();?>

<? endif; ?>