<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?=$this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"><? # Tell the browser to be responsive to screen width ?>
        <?=$this->Html->meta('icon') ?>
        
        <title><?=$this->fetch('title');?></title>
      
        <?=$this->Html->css('bootstrap.min.css'); # Bootstrap 3.3.6 ?>
        <?=$this->Html->css('font-awesome.min.css'); # Font Awesome ?>
        <?=$this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'); # Ionicons ?>
        <?#=$this->Html->css('/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); # jvectormap ?>
        <?=$this->Html->css('/dist/css/AdminLTE.min.css'); # Theme style ?>
        <?=$this->Html->css('/dist/css/skins/_all-skins.min.css'); # AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. ?>
        <?=$this->Html->css('bootstrap-datepicker.css'); # Bootstrap Datepicker ?>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <?=$this->Html->script('/plugins/jQuery/jquery-2.2.3.min.js'); # jQuery 2.2.3 ?>
        <?=$this->Html->script('bootstrap.min.js'); # Bootstrap 3.3.6 ?>
        <?=$this->Html->script('/plugins/fastclick/fastclick.js'); # FastClick ?>
        <?=$this->Html->script('/dist/js/app.min.js'); # AdminLTE App ?>
        <?#=$this->Html->script('/plugins/sparkline/jquery.sparkline.min.js'); # Sparkline ?>
        <?#=$this->Html->script('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); # jvectormap ?>
        <?#=$this->Html->script('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); # jvectormap ?>
        <?=$this->Html->script('/plugins/slimScroll/jquery.slimscroll.min.js'); # SlimScroll 1.3.0 ?>
        <?#=$this->Html->script('/plugins/chartjs/Chart.min.js'); # ChartJS 1.0.1 ?>
        <?#=$this->Html->script('/dist/js/pages/dashboard2.js'); # AdminLTE dashboard demo (This is only for demo purposes) ?>
        <?#=$this->Html->script('/dist/js/demo.js'); # AdminLTE for demo purposes ?>
        <?=$this->Html->script('bootstrap-datepicker.min.js'); # Jquery Datepicker ?>
        <?=$this->Html->script('admin_oxus'); # Custom Functions ?>
        
        <?=$this->fetch('meta') ?>
        <?=$this->fetch('css') ?>
        <?=$this->fetch('script') ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?=$this->element('Users.header');?>
            <?=$this->element('Users.sidebar');?>
            <div class="content-wrapper">
                <?=$this->fetch('content');?>
            </div>
        </div>
    </body>
</html>