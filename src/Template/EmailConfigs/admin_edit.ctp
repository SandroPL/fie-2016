<?php $this->assign('title', 'Admin FIE | Configuración de Correos'); ?>
<section class="content-header">
    <h1 class="page-header"><?=__('Editar Configuración de Correo');?></h1>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),['plugin' => 'Users', 'controller' => 'Users', 'action' => 'adminDashboard']);?>
        </li>
        <li>
            <i class="fa fa-envelope-o"></i>
            <?=$this->Html->link(__('Configuración correos'),['controller' => 'EmailConfigs', 'action' => 'adminEdit']);?>
        </li>
        <li class="active">
            <i class="fa fa-edit"></i> <?=__('Editar Configuración');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-8 col-md-8">
            <?=$this->Flash->render();?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                </div>
                <?=$this->Form->create($emailConfig,[
                    'novalidate' => true,
                    'templates' => [
                        'inputContainer' => '
                            <div class="form-group {{errorClass}}">
                                {{content}}
                                {{errorMessage}}
                            </div>'
                    ]
                ]);?>
                <div class="box-body">
                    <?php
                        # Email
                        echo $this->Form->input('email',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Email')
                                ],
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('email') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('email')
                                ],
                                'class' => 'form-control',
                                'error' => false
                        ]);
                        
                        # Contraseña
                        echo $this->Form->input('password',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Contraseña')
                                ],
                                'class' => 'form-control',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('password') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('password')
                                ],
                                'error' => false
                        ]);
                        
                        # Protocolo
                        echo $this->Form->input('protocol',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Protocolo de Transporte')
                                ],
                                'class' => 'form-control',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('protocol') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('protocol')
                                ],
                                'error' => false
                        ]);
                        
                        # Desde
                        echo $this->Form->input('from',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('De:')
                                ],
                                'class' => 'form-control',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('from') ? 'has-error' : ''),
                                    'errorMessage' => ''
                                ],
                                'error' => false
                        ]);
                        
                        # Host
                        echo $this->Form->input('host',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Host')
                                ],
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('host') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('host')
                                ],
                                'class' => 'form-control',
                                'error' => false
                        ]);
                        
                        # Puerto
                        echo $this->Form->input('port',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Puerto')
                                ],
                                'class' => 'form-control',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('port') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('port')
                                ],
                                'error' => false
                        ]);
                        echo $this->Form->submit(__('Guardar'),['class' => 'btn btn-success', 'div' => false]);
                    ?>
                </div>
                <?= $this->Form->end();?>
            </div>
        </div>
    </div>
</section>