<?php $this->assign('title', 'Admin FIE | Regiones'); ?>
<section class="content-header">
    <h1 class="page-header"><?=__('Regiones');?></h2>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),['plugin' => 'Users', 'controller' => 'Users','action' => 'adminDashboard']);?>
        </li>
        <li class="active">
            <?=__('Regiones');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?=$this->Flash->render();?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?=__('Listado Regiones');?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <style type="text/css">
                                thead th { color: #3c8dbc; }
                            </style>
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <th><?= $this->Paginator->sort('id',__('#')) ?></th>
                                    <th><?= __('Nombre') ?></th>
                                    <th class="actions text-center"><?= __('Acciones') ?></th>
                                </thead>
                                <tbody>
                                    <?php foreach ($regions as $region): ?>
                                    <tr>
                                        <td><?= $this->Number->format($region->id) ?></td>
                                        <td><?= $region->name ?></td>
                                        <td class="actions" style="text-align: center">
                                            <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                                        ['action' => 'edit', $region->id],
                                                        ['escape' => false,'title' => 'Editar']);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>