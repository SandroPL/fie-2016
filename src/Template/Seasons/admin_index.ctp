<?php $this->assign('title', 'Admin FIE | Temporadas'); ?>
<section class="content-header">
    <h1 class="page-header"><?=__('Temporadas');?></h2>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),['plugin' => 'Users', 'controller' => 'Users','action' => 'adminDashboard']);?>
        </li>
        <li class="active">
            <i class="fa fa-users"></i> <?=__('Temporadas');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?=$this->Flash->render();?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?=__('Listado Temporadas');?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <style type="text/css">
                                thead th { color: #3c8dbc; }
                            </style>
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <th><?= __('#') ?></th>
                                    <th><?= __('Fecha de inicio') ?></th>
                                    <th><?= __('Fecha de término') ?></th>
                                    <th class="actions text-center"><?= __('Acciones') ?></th>
                                </thead>
                                <tbody>
                                    <?php foreach ($seasons as $season): ?>
                                    <tr>
                                        <td><?= $this->Number->format($season->id) ?></td>
                                        <td><?= $season->init_date->format('d-m-Y') ?></td>
                                        <td><?= $season->finish_date->format('d-m-Y') ?></td>
                                        <td class="actions" style="text-align: center">
                                            <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                                        ['action' => 'adminEdit',$season->id],
                                                        ['escape' => false,'title' => 'Editar']);
                                            ?>
                                            <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',
                                                        ['action' => 'adminDelete', $season->id],
                                                        ['escape' => false, 'title' => 'Eliminar',
                                                         'confirm' => __('Estás seguro de eliminar ID # {0}?', $season->id)]);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>