<section class="content-header">
    <h1 class="page-header"><?=__('Crear Temporada');?></h1>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),['plugin' => 'Users', 'controller' => 'Users','action' => 'adminDashboard']);?>
        </li>
        <li>
            <i class="fa fa-hourglass-o"></i>
            <?=$this->Html->link(__('Temporadas'),['action' => 'adminIndex']);?>
        </li>
        <li class="active">
            <i class="fa fa-edit"></i> <?=__('Crear Temporada');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-8 col-md-8">
            <?=$this->Flash->render();?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                </div>
                <?=$this->Form->create($season,[
                    'novalidate' => true,
                    'templates' => [
                        'inputContainer' => '
                            <div class="form-group {{errorClass}} col-xs-6">
                                {{content}}
                                {{errorMessage}}
                            </div>'
                    ]
                ]);?>
                <div class="box-body">
                    <?php
                        # Nombre de Usuario
                        echo $this->Form->input('init_date',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Fecha de Inicio')
                                ],
                                'class' => 'form-control datepicker',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('init_date') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('init_date')
                                ],
                                'error' => false,
                                'type' => 'text'
                        ]);
                        
                        # Contraseña
                        echo $this->Form->input('finish_date',[
                                'label' => [
                                    'class' => 'control-label',
                                    'text' => __('Fecha de término')
                                ],
                                'class' => 'form-control datepicker',
                                'templateVars' => [
                                    'errorClass' => ($this->Form->isFieldError('finish_date') ? 'has-error' : ''),
                                    'errorMessage' => $this->Form->error('finish_date')
                                ],
                                'error' => false,
                                'type' => 'text'
                        ]);
                        echo $this->Form->submit(__('Guardar'),['class' => 'btn btn-success', 'div' => false]);
                    ?>
                </div>
                <?= $this->Form->end();?>
            </div>
        </div>
    </div>
</section>