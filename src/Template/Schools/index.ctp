<?=$this->Html->script('/plugins/jQuery/jquery-2.2.3.min.js'); # jQuery 2.2.3 ?>

<?=$this->Form->create($school,[
    'novalidate' => false,
    'style' => 'width: 50%;'
]);?>
    <?=$this->Form->input('rbd',[
        'label' => 'Ingrese RBD:',
        'id' => 'SchoolRBD',
        'div' => false
    ]);?>
    <?=$this->Form->button('Buscar:',[
        'class' => 'btnBuscar',
        'style' => 'width: 20%;'
    ]);?>
    <?=$this->Form->input('name');?>
    <?=$this->Form->input('dependence');?>
    <?=$this->Form->input('geographical_area');?>
    <?=$this->Form->input('region_id',[
        'type' => 'text'
    ]);?>
    <?=$this->Form->input('province_id',[
        'type' => 'text'
    ]);?>
    <?=$this->Form->input('district_id',[
        'type' => 'text'
    ]);?>
<?=$this->Form->end();?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.btnBuscar').click(function(e){
            e.preventDefault();
            var rbd = $('#SchoolRBD').val();
            $.ajax({
                url: '/schools/findByRbd/'+rbd,
                success: function(response){
                    var json_obj = JSON.parse(response);
                    $.each(json_obj,function(key,value){
                        switch(key){
                            case 'region':
                            case 'province':
                            case 'district':
                                $('input[name='+key+'_id]').val(value.name);
                                break;
                            default:
                                $('input[name='+key+']').val(value);
                                break;
                        }
                    });
                }
            });
        });
    });
</script>