<?php $this->assign('title', 'Admin FIE | Instituciones'); ?>
<section class="content-header">
    <h1 class="page-header"><?=__('Instituciones');?></h2>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),['plugin' => 'Users', 'controller' => 'Users','action' => 'adminDashboard']);?>
        </li>
        <li class="active">
            <i class="fa fa-users"></i> <?=__('Instituciones');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?=$this->Flash->render();?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?=__('Listado Instituciones');?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <style type="text/css">
                                thead th { color: #3c8dbc; }
                            </style>
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <th><?= __('RBD') ?></th>
                                    <th><?= __('Nombre') ?></th>
                                    <th><?= __('Comuna');?></th>
                                    <th><?= __('Provincia');?></th>
                                    <th><?= __('Región');?></th>
                                    <th class="actions text-center"><?= __('Acciones') ?></th>
                                </thead>
                                <tbody>
                                    <?php foreach ($schools as $school): ?>
                                    <tr>
                                        <td><?= $this->Number->format($school->rbd) ?></td>
                                        <td><?= h($school->name) ?></td>
                                        <td><?= $school->district->name ?></td>
                                        <td><?= $school->province->name ?></td>
                                        <td><?= $school->region->name ?></td>
                                        <td class="actions" style="text-align: center">
                                            <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                                        ['action' => 'adminEdit',$school->id],
                                                        ['escape' => false,'title' => 'Editar']);
                                            ?>
                                            <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',
                                                        ['action' => 'adminDelete', $school->id],
                                                        ['escape' => false, 'title' => 'Eliminar',
                                                         'confirm' => __('Estás seguro de eliminar ID # {0}?', $school->id)]);
                                            ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>