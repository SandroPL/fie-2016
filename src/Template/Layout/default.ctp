<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes;" name="viewport">
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=BioRhyme:400,300,700">
        
        <meta content="Fondo para Iniciativas Escolares" name="description">
        <meta content="FIE" name="author">
    
        <?=$this->Html->script('jquery-1.11.3.min.js');?>
        <?=$this->Html->script('global');?>
        <?=$this->Html->css('style');?>
    
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
        <title><?= $this->fetch('title') ?></title>
    </head>
    <body>
        <div class="container">
            <?=$this->Element('layout/header');?>
            <section class="breadcrumb">
                
            </section>
            <section class="content">
                <?=$this->Element('layout/aside');?>
                <?=$this->fetch('content');?>
            </section>
        </div>
        <?=$this->Element('layout/footer');?>
    </body>
</html>
