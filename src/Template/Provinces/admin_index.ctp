<?php $this->assign('title', 'Admin FIE | Provincias'); ?>
<section class="content-header">
    <h1 class="page-header"><?=__('Provincias');?></h2>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),'/users/users/admin-dashboard');?>
        </li>
        <li class="active">
            <?=__('Provincias');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?=$this->Flash->render();?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?=__('Listado Provincias');?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <style type="text/css">
                                thead th { color: #3c8dbc; }
                            </style>
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <th><?= $this->Paginator->sort('id','ID') ?></th>
                                    <th><?= $this->Paginator->sort('region_id','Región') ?></th>
                                    <th><?= $this->Paginator->sort('name','Nombre') ?></th>
                                    <th class="actions"><?= __('Acciones') ?></th>
                                </thead>
                                <tbody>
                                    <?php foreach ($provinces as $province): ?>
                                    <tr>
                                        <td><?= $this->Number->format($province->id) ?></td>
                                        <td><?= $province->has('region') ? $province->region->name : '' ?></td>
                                        <td><?= h($province->name) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                                    ['action' => 'adminEdit', $province->id],[
                                                     'escape' => false])
                                            ?>
                                            <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',
                                                    ['action' => 'adminDelete', $province->id],[
                                                     'escape' => false,
                                                     'confirm' => __('Estás seguro(a) de eliminar la provincia de {0}?', $province->name)])
                                            ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>