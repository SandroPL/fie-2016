<?php $this->assign('title', 'Admin FIE | Comunas'); ?>
<section class="content-header">
    <h1 class="page-header"><?=__('Comunas');?></h2>
    <ol class="breadcrumb">
        <li>
            <i class="fa fa-dashboard"></i>
            <?=$this->Html->link(__('Dashboard'),['plugin' => 'Users', 'controller' => 'Users','action' => 'adminDashboard']);?>
        </li>
        <li class="active">
            <i class="fa fa-users"></i> <?=__('Comunas');?>
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?=$this->Flash->render();?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?=__('Listado Comunas');?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <style type="text/css">
                                thead th { color: #3c8dbc; }
                            </style>
                            <table class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id','ID') ?></th>
                                        <th><?= $this->Paginator->sort('name',__('Nombre')) ?></th>
                                        <th><?= $this->Paginator->sort('province_id',__('Provincia')) ?></th>
                                        <th><?= $this->Paginator->sort('region_id',__('Región')) ?></th>
                                        <th class="actions"><?= __('Acciones') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($districts as $district): ?>
                                    <tr>
                                        <td><?= $this->Number->format($district->id) ?></td>
                                        <td><?= h($district->name) ?></td>
                                        <td><?= $district->has('province') ? $district->province->name : '' ?></td>
                                        <td><?= $district->province->region->name ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-edit" aria-hidden="true"></i>',
                                                    ['action' => 'adminEdit', $district->id],[
                                                     'escape' => false])
                                            ?>
                                            <?= $this->Form->postLink('<i class="fa fa-trash" aria-hidden="true"></i>',
                                                    ['action' => 'adminDelete', $district->id],[
                                                     'escape' => false,
                                                     'confirm' => __('Estás seguro(a) de eliminar la comuna {0}?', $district->name)])
                                            ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>