<header>
    <section class="header-top">
        <?=$this->Html->image('logo.png',['class' => 'logo', 'alt' => __('FIE logo')]);?>
        <div class="line-block"></div>
        <h1 class="tit-page"><b><?=__('FIE');?></b> - <?=__('Fondo para Iniciativas Escolares');?></h1>  
        <nav>
            <h2><?=__('Bienvenido(a)');?> <br/> <b><?=$logged_user->username;?></b></h2>
            <ul>
                <li><?=$this->Html->link(__('> Mis datos'),['plugin' => 'Users', 'controller' => 'Users', 'action' => 'profile', 'edit' => false]);?></li>
                <li><?=$this->Html->link(__('> Cambio de clave'),['plugin' => 'Users', 'controller' => 'Users', 'action' => 'editPassword']);?></li>
                <li><?=$this->Html->link(__('> Cerrar sesión'),['plugin' => 'Users', 'controller' => 'Users', 'action' => 'logout']);?></li>
            </ul>
        </nav>
    </section>
</header>
<section class="iconsTop">
    <?=$this->Html->image('icons.png',['alt' => __('Iconos Inicio')]);?>
    <?=$this->Html->link(__('Postula tu idea. <b>¡aquí!</b>'),['plugin' => 'Projects', 'controller' => 'Projects', 'action' => 'step_one'],['title' => __('Postular'), 'escape' => false]);?>
</section>