<footer>
    <a href="#" title="Abrir..." class="get-document"><span class="pdf"></span><?=__('Bases de Postulación');?></a>
    <div class="info-footer">
        <ul>
            <li>Alcántara 44 piso 3</li>
            <li>Las Condes, Santiago</li>
            <li>Fono: +56 2 22286967</li>
            <li><a href="mailto:informaciones@fundacionluksic.cl" title="Escribir">informaciones@fundacionluksic.cl</a></li>
            <li><a href="http://www.fundacionluksic.cl" title="Ir a fundación luksic">www.fundacionluksic.cl</a></li>
        </ul>
        <?=$this->Html->image('logo-footer.png',['alt' => __('Logo Luksic')]);?>
    </div>
</footer>