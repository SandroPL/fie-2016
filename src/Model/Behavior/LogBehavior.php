<?php

namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;

class LogBehavior extends Behavior{
    
    /**
     * Protected var $_defaultConfig
     */
    protected $_defaultConfig = [
        'user' => 0,            # ID del usuario que genera el log
        'useTable' => 'Logs',   # tabla que será usada para guardar los logs
        'messageCreate' => '',  # mensage que se mostrará al generar un log de creación
        'messageUpdate' => '',  # mensage que se mostrará al generar un log de edición
        'messageDelete' => '',  # mensage que se mostrará al generar un log de eliminación
        'implementedMethods' => [
			'setUserToLog' => 'setUserToLog', # método implementado en la tabla anfitriona para setear el ID del usuario
		],
        'plugin' => false       # plugin de la tabla anfitriona
    ];
    
    /**
     * afterSave Callback
     *
     * @param Event $event
     * @param EntityInterface $entity
     *
     * return void
     */
    public function afterSave(Event $event, EntityInterface $entity){
        if($event->subject()->alias() == 'Users' && $entity->dirty('status')){
            $event->stopPropagation();
            return;
        }
        $this->createLog($event->name(),$event->subject()->alias(),$entity);
    }
    
    /**
     * afterDelete Callback
     *
     * @param Event $event
     * @param EntityInterface $entity
     *
     * return void
     */
    public function afterDelete(Event $event, EntityInterface $entity){
        $this->createLog($event->name(),$event->subject()->alias(),$entity);
    }
    
    /**
     * createLog function
     *
     * @param string $eventName ("Model.afterSave" or "Model.afterDelete")
     * @param EntityInterface $entity
     *
     * return void
     */
    public function createLog($eventName, $model, EntityInterface $entity){
        if($eventName == 'Model.afterDelete'){
            $action = 'delete';
            $message = $this->config('messageDelete');
        } else {
            $action = $entity->isNew() ? 'create' : 'update';
            $message = $entity->isNew() ? $this->config('messageCreate') : $this->config('messageUpdate');
        }
        
        $tableName = !$this->config('plugin') ? $this->config('useTable') : $this->config('plugin').'.'.$this->config('useTable');
        $tableLog = $this->getTableInstance($tableName);
        $log = $tableLog->newEntity([
            'model' => $model,
            'associated_id' => $entity->id,
            'action' => $action,
            'message' => $message,
            'status' => 0,
            'user_id' => $this->config('user'),
            'json_data' => json_encode($entity),
            'created' => date('Y-m-d H:i:s')
        ]);
        
        $tableLog->save($log);
    }
    
    public function setUserToLog($user_id = null){
        if(isset($user_id)){
            $this->_config['user'] = $user_id;
        }
    }
    
    function getTableInstance($table){
        $obj = TableRegistry::get($table);
        return $obj;
    }
}