<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Seasons Model
 *
 * @method \App\Model\Entity\Season get($primaryKey, $options = [])
 * @method \App\Model\Entity\Season newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Season[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Season|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Season patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Season[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Season findOrCreate($search, callable $callback = null)
 */
class SeasonsTable extends Table{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config){
        parent::initialize($config);

        $this->table('seasons');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->addBehavior('Log',[
            'messageCreate' => __('Se ha creado nueva temporada.'),
            'messageUpdate' => __('Se ha modificado una temporada.'),
            'messageDelete' => __('Se ha eliminado una temporada')
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator){
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('init_date')
            ->requirePresence('init_date', 'true')
            ->notEmpty('init_date','Debe escoger una fecha de inicio.')
            ->add('init_date', 'custom', [
                'rule' => function ($value, $context){
                    if(!empty($context['data']['init_date']) && !empty($context['data']['finish_date'])){
                        if(strtotime($value) > strtotime($context['data']['finish_date']))
                            return false;
                    }
                    return true;
                },
                'message' => 'La fecha inicial no puede ser mayor a la fecha de término.'
            ]);
        $validator
            ->date('finish_date')
            ->requirePresence('finish_date', 'true')
            ->notEmpty('finish_date','Debe escoger una fecha de término.');

        return $validator;
    }
}