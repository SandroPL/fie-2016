<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * School Entity
 *
 * @property int $id
 * @property int $institution_region_id
 * @property int $institution_province_id
 * @property int $institution_district_id
 * @property string $name
 * @property string $rbd
 * @property string $dependence
 * @property string $sostenedor
 * @property string $geographical_area
 * @property string $ive_sinae
 * @property bool $visible
 *
 * @property \App\Model\Entity\InstitutionRegion $institution_region
 * @property \App\Model\Entity\InstitutionProvince $institution_province
 * @property \App\Model\Entity\InstitutionDistrict $institution_district
 * @property \App\Model\Entity\Project[] $projects
 */
class School extends Entity{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
