<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Districts Controller
 *
 * @property \App\Model\Table\DistrictsTable $Districts
 */
class DistrictsController extends AppController{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function adminIndex(){
        $this->paginate = [
            'contain' => ['Provinces' => ['Regions']]
        ];
        $districts = $this->paginate($this->Districts);
        $this->set(compact('districts'));
        $this->set('_serialize', ['districts']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function adminAdd(){
        $district = $this->Districts->newEntity();
        if ($this->request->is('post')) {
            $district = $this->Districts->patchEntity($district, $this->request->data);
            if ($this->Districts->save($district)) {
                $this->Flash->success(__('La comuna ha sido guardada.'));

                return $this->redirect(['action' => 'adminIndex']);
            } else {
                $this->Flash->error(__('La comuna no pudo ser guardada. Por favor, intente nuevamente.'));
            }
        }
        $provinces = $this->Districts->Provinces->find('list', ['limit' => 200]);
        $this->set(compact('district', 'provinces'));
        $this->set('_serialize', ['district']);
    }

    /**
     * Edit method
     *
     * @param string|null $id District id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function adminEdit($id = null){
        $district = $this->Districts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $district = $this->Districts->patchEntity($district, $this->request->data);
            if ($this->Districts->save($district)) {
                $this->Flash->success(__('La comuna ha sido guardada.'));

                return $this->redirect(['action' => 'adminIndex']);
            } else {
                $this->Flash->error(__('La comuna no pudo ser guardada. Por favor, intente nuevamente.'));
            }
        }
        $provinces = $this->Districts->Provinces->find('list', ['limit' => 200]);
        $this->set(compact('district', 'provinces'));
        $this->set('_serialize', ['district']);
    }

    /**
     * Delete method
     *
     * @param string|null $id District id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function adminDelete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $district = $this->Districts->get($id);
        if ($this->Districts->delete($district)) {
            $this->Flash->success(__('La comuna ha sido eliminada.'));
        } else {
            $this->Flash->error(__('La comuna no pudo ser eliminada. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'adminIndex']);
    }
}
