<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Schools Controller
 *
 * @property \App\Model\Table\SchoolsTable $Schools
 */
class SchoolsController extends AppController{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function adminIndex(){
        $this->paginate = [
            'contain' => ['Districts','Provinces','Regions']
        ];
        $schools = $this->paginate($this->Schools);
        
        $this->set(compact('schools'));
        $this->set('_serialize', ['schools']);
    }

    public function index(){
         $school = $this->Schools->newEntity();
         
         $this->set('school',$school);
         $this->set('_serialize',['school']);
    }

    public function findByRbd($rbd){
        $this->viewBuilder()->layout('ajax');
        $school = $this->Schools->find('all')
            ->where(['rbd' => $rbd])
            ->contain(['Provinces','Districts','Regions'])
            ->first()
            ->toArray();
        echo json_encode($school);
    }

}