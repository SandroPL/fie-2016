<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Seasons Controller
 *
 * @property \App\Model\Table\SeasonsTable $Seasons
 */
class SeasonsController extends AppController{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function adminIndex(){
        $seasons = $this->paginate($this->Seasons);

        $this->set(compact('seasons'));
        $this->set('_serialize', ['seasons']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function adminAdd(){
        $season = $this->Seasons->newEntity();
        if ($this->request->is('post')) {
            $season = $this->Seasons->patchEntity($season, $this->request->data);
            if($this->Seasons->hasBehavior('Log')){
                $this->Seasons->setUserToLog($this->Auth->user('id'));
            }
            if ($this->Seasons->save($season)) {
                $this->Flash->success(__('La temporada ha sido guardada.'));

                return $this->redirect(['action' => 'adminIndex']);
            } else {
                $this->Flash->error(__('La temporada no ha podido ser guardada. Por favor, intente nuevamente.'));
            }
        }
        $this->set(compact('season'));
        $this->set('_serialize', ['season']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Season id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function adminEdit($id = null){
        $season = $this->Seasons->get($id, [
            'contain' => []
        ]);
        $season->init_date = $season->init_date->format('Y-m-d');
        $season->finish_date = $season->finish_date->format('Y-m-d');
        if($this->Seasons->hasBehavior('Log')){
            $this->Seasons->setUserToLog($this->Auth->user('id'));
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $season = $this->Seasons->patchEntity($season,[
                    'init_date' => $this->request->data['init_date'],
                    'finish_date' => $this->request->data['finish_date']
            ]);
            if ($this->Seasons->save($season)) {
                $this->Flash->success(__('La temporada ha sido guardada.'));

                return $this->redirect(['action' => 'adminIndex']);
            } else {
                $this->Flash->error(__('La temporada no ha podido ser guardada. Por favor, intente nuevamente.'));
            }
        }
        $this->set(compact('season'));
        $this->set('_serialize', ['season']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Season id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function adminDelete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $season = $this->Seasons->get($id);
        if($this->Seasons->hasBehavior('Log')){
            $this->Seasons->setUserToLog($this->Auth->user('id'));
        }
        if ($this->Seasons->delete($season)) {
            $this->Flash->success(__('La temporada ha sido eliminada.'));
        } else {
            $this->Flash->error(__('La temporada no pude ser eliminada. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'adminIndex']);
    }
}