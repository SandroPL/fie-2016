<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EmailConfigs Controller
 *
 * @property \App\Model\Table\EmailConfigsTable $EmailConfigs
 */
class EmailConfigsController extends AppController{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){
        $emailConfigs = $this->paginate($this->EmailConfigs);

        $this->set(compact('emailConfigs'));
        $this->set('_serialize', ['emailConfigs']);
    }

    /**
     * View method
     *
     * @param string|null $id Email Config id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null){
        $emailConfig = $this->EmailConfigs->get($id, [
            'contain' => []
        ]);

        $this->set('emailConfig', $emailConfig);
        $this->set('_serialize', ['emailConfig']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add(){
        $emailConfig = $this->EmailConfigs->newEntity();
        if ($this->request->is('post')) {
            $emailConfig = $this->EmailConfigs->patchEntity($emailConfig, $this->request->data);
            if ($this->EmailConfigs->save($emailConfig)) {
                $this->Flash->success(__('The email config has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The email config could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('emailConfig'));
        $this->set('_serialize', ['emailConfig']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Email Config id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function adminEdit(){
        $emailConfig = $this->EmailConfigs->find('all')->first();
        unset($emailConfig->password);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailConfig = $this->EmailConfigs->patchEntity($emailConfig, $this->request->data);
            if ($this->EmailConfigs->save($emailConfig)) {
                $this->Flash->success(__('Configuración guardada.'));
            } else {
                $this->Flash->error(__('Ha ocurrido un error al guardar. Por favor, intente nuevamente.'));
            }
        }
        $this->set(compact('emailConfig'));
        $this->set('_serialize', ['emailConfig']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Email Config id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $emailConfig = $this->EmailConfigs->get($id);
        if ($this->EmailConfigs->delete($emailConfig)) {
            $this->Flash->success(__('The email config has been deleted.'));
        } else {
            $this->Flash->error(__('The email config could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}