<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth',[
            'authorize' => ['Controller'],
            'authenticate' => [
                'Form' => [
                    //solo los usuarios activos se pueden loguear
                    'scope' => ['active' => '1'],
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'plugin' => 'Users',
                'controller' => 'Users',
                'action' => 'login',
            ],
            'loginRedirect' => '/mis-datos',
            'logoutRedirect' => '/login'
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event){
        if($this->request->is('ajax')){
            # seteamos el layout a usar
            $this->viewBuilder()->layout = 'ajax';
            
        }elseif(substr($this->request->params['action'],0,5) == 'admin' || $this->request->params['action'] == 'login'){
            $login_actions = ['login','admin_requestNewPassword','admin_changePassword'];
            $layout = (in_array($this->request->params['action'],$login_actions)) ? 'Users.login' : 'Users.admin';
            $this->viewBuilder()->layout($layout);
            
            Configure::load('admin_sidebar');
            $this->set('sidebar',Configure::read('Sidebar'));
        }
        
        if (!array_key_exists('_serialize', $this->viewVars) && in_array($this->response->type(), ['application/json', 'application/xml'])){
            $this->set('_serialize', true);
        }
        
        # Enviamos la variable $logged_user a todas las vistas. Ésta contiene la información del usuario logueado.
        if(!empty($this->Auth->user())){
            $usersTable = $this->getTableInstance('Users.Users');
            $logged_user = $usersTable->find('all',[
                'conditions' => ['Users.id' => $this->Auth->user('id')],
                'contain' => ['Userinfos']
            ])->first();
            $this->set('logged_user',$logged_user);
        }
    }
    
    /**
     * Before filter callback.
     *
     * @param \Cake\Event\Event $event The beforeFilter event.
     * @return void
     */
    public function beforeFilter(Event $event){
        
    }
    
    public function isAuthorized($user){
        $return = true;
        if(substr($this->request->params['action'],0,5) == 'admin'){
            $return = $this->Auth->user('is_admin') == 1 ? true : false;
        }
        return $return;
    }
    
    /**
     * getEmailInstance function
     *
     * Obtiene una instancia de CakeEmail() para poder enviar correos.
     *
     * @return object Email()
     */
    public function getEmailInstance(){
        Email::configTransport('gmail', [
            'host' => 'ssl://smtp.gmail.com',
            'port' => 465,
            'username' => 'nicolas.recabarren@oxus.cl',
            'password' => 'Nprv.1997',
            'className' => 'Smtp'
        ]);
        return new Email();
    }
    
    /**
     * getTableInstance function
     *
     * Obtiene una instancia de una clase que viene por parámetro.
     *
     * @param string $table (nombre de la clase a instanciar)
     * @return object $obj
     */
    public function getTableInstance($table){
        $obj = TableRegistry::get($table);
        return $obj;
    }
}
