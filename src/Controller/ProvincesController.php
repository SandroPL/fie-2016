<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Provinces Controller
 *
 * @property \App\Model\Table\ProvincesTable $Provinces
 */
class ProvincesController extends AppController{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function adminIndex(){
        $this->paginate = [
            'contain' => ['Regions']
        ];
        $provinces = $this->paginate($this->Provinces);

        $this->set(compact('provinces'));
        $this->set('_serialize', ['provinces']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function adminAdd(){
        $province = $this->Provinces->newEntity();
        if ($this->request->is('post')) {
            $province = $this->Provinces->patchEntity($province, $this->request->data);
            if ($this->Provinces->save($province)) {
                $this->Flash->success(__('La provincia ha sido guardada.'));

                return $this->redirect(['action' => 'adminIndex']);
            } else {
                $this->Flash->error(__('La provincia no pudo ser guardada. Por favor, intente nuevamente.'));
            }
        }
        $regions = $this->Provinces->Regions->find('list', ['limit' => 200]);
        $this->set(compact('province', 'regions'));
        $this->set('_serialize', ['province']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Province id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function adminEdit($id = null){
        $province = $this->Provinces->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $province = $this->Provinces->patchEntity($province, $this->request->data);
            if ($this->Provinces->save($province)) {
                $this->Flash->success(__('La provincia ha sido guardada.'));

                return $this->redirect(['action' => 'adminIndex']);
            } else {
                $this->Flash->error(__('La provincia no pudo ser guardada. Por favor, intente nuevamente.'));
            }
        }
        $regions = $this->Provinces->Regions->find('list', ['limit' => 200]);
        $this->set(compact('province', 'regions'));
        $this->set('_serialize', ['province']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Province id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function adminDelete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $province = $this->Provinces->get($id);
        if ($this->Provinces->delete($province)) {
            $this->Flash->success(__('La provincia ha sido eliminada.'));
        } else {
            $this->Flash->error(__('La provincia no pudo ser eliminada. Por favor, intente nuevamente.'));
        }

        return $this->redirect(['action' => 'adminIndex']);
    }
}
