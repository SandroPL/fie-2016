<?php
$breadcrumb = [
    'Inicio' => [
        #el índice "url" sirve para indicar la ruta hacia lo que esté arriba de él 
        'url' => ['plugin' => 'Projects', 'controller' => 'Projects', 'action' => 'index'],
        #el índice "map" se usa para verificar si desde él se puede acceder a más páginas no mencionadas anteriormente
        'map' => [
            'Mis Datos' => [
                'url' => ['plugin' => 'Users', 'controller' => 'Users', 'action' => 'profile', 'edit' => false],
                'map' => [
                    'Cambiar Clave' => [
                        'url' => ['plugin' => 'Users', 'controller' => 'Users', 'action' => 'editPassword'],
                        'map' => false
                    ],
                    'Editar' => [
                        'url' => ['plugin' => 'Users', 'controller' => 'Users', 'action' => 'profile', 'edit' => false],
                        'map' => false
                    ]
                ]
            ],
            'Postular a Proyecto' => [
                
            ],
        ]
    ]
];