<?php
$config = [
    'Sidebar' => [
        /*0 => [
            'props' => [ # Propiedades
                'name' => 'Default', # nombre
                'ico' => false, # ícono (se obtiene desde font-awesome)
                'target' => false, # id de data-target (solo modificar si tiene submenú)
                'url' => false, # modificar solo si redirigirá al clickear en la pestaña principal del submenú
                'current' => [ # current se usa para agregar la clase "active" a la pestaña escogida
                    'controller' => false, # controller actual
                    'plugin' => false # plugin actual, si no tiene dejar en false
                ]
            ],
            'options-list' => false, # si no tiene submenú dejar en false
            'options-list' => [
                0 => [
                    'name' => '', # nombre a desplegar en submenú
                    'url' => '', # url de submenú
                    'current' => [ # current se usa para la clase active
                        'action' => '', # vista de submenú
                        'controller' => '', # controller de submenú
                        'plugin' => false # plugin de submenú
                    ]
                ],
                ...
            ]
        ],*/
        0 => [
            'props' => [
                'name' => 'Usuarios',
                'ico' => 'fa-users',
                'target' => 'sidebar-users',
                'url' => false,
                'current' => ['controller' => 'Users','plugin' => 'Users']
            ],
            'options-list' => [[
                    'name' => 'Listado',
                    'url' => '/users/users/admin-index',
                    'current' => ['action' => 'adminIndex','controller' => 'Users','plugin' => 'Users']
                ],[
                    'name' => 'Crear',
                    'url' => '/users/users/admin-add',
                    'current' => ['action' => 'adminAdd','controller' => 'Users','plugin' => 'Users']
                ]
            ]
        ],
        1 => [
            'props' => [
                'name' => 'Proyectos',
                'ico' => 'fa-file-text',
                'target' => 'sidebar-projects',
                'url' => false,
                'current' => ['controller' => 'Projects','plugin' => 'Projects']
            ],
            'options-list' => [[
                    'name' => 'En Proceso',
                    'url' => '/projects/projects/admin-index/in-progress',
                    'current' => ['action' => '','controller' => 'Projects','plugin' => 'Projects']
                ],[
                    'name' => 'Enviadas',
                    'url' => '/projects/projects/admin-index/sended',
                    'current' => ['action' => '', 'controller' => 'Projects', 'plugin' => 'Projects']
                ],[
                    'name' => 'Aprobados',
                    'url' => '/projects/projects/admin-index/approved',
                    'current' => ['action' => '', 'controller' => 'Projects', 'plugin' => 'Projects']
                ],[
                    'name' => 'Rechazadas',
                    'url' => '/projects/projects/admin-index/rejected',
                    'current' => ['action' => '', 'controller' => 'Projects', 'plugin' => 'Projects']
                ]
            ]
        ],
        2 => [
            'props' => [
                'name' => 'Temporadas',
                'ico' => 'fa-hourglass',
                'target' => 'sidebar-seasons',
                'url' => false,
                'current' => ['controller' => 'Seasons',]
            ],
            'options-list' => [[
                    'name' => 'Listado',
                    'url' => '/seasons/admin-index',
                    'current' => ['action' => 'adminIndex','controller' => 'Seasons','plugin' => false]
                ],[
                    'name' => 'Crear',
                    'url' => '/seasons/admin-add',
                    'current' => ['action' => 'add','controller' => 'Seasons','plugin' => false]
                ]
            ]
        ],
        3 => [
            'props' => [
                'name' => 'Configuración SMTP',
                'ico' => 'fa-envelope',
                'target' => 'sidebar-conf-email',
                'url' => '/email-configs/admin-edit',
                'current' => ['controller' => 'ConfigEmails']
            ],
            'options-list' => false
        ],
        4 => [
            'props' => [
                'name' => 'Instituciones',
                'ico' => 'fa-university',
                'target' => 'sidebar-schools',
                'url' => '/schools/admin-index',
                'current' => ['controller' => 'Schools','plugin' => false]
            ],
            'options-list' => false
        ],
        5 => [
            'props' => [
                'name' => 'Mantenedores',
                'ico' => '',
                'target' => 'sidebar-geographical',
                'url' => false,
                'current' => ['controller' => 'Schools','plugin' => false]
            ],
            'options-list' => [[
                    'name' => 'Regiones',
                    'url' => '/regions/admin-index',
                    'current' => ['action' => 'adminIndex','controller' => 'Regions','plugin' => false]
                ],[
                    'name' => 'Provincias',
                    'url' => '/provinces/admin-index',
                    'current' => ['action' => 'adminIndex','controller' => 'Provinces','plugin' => false]
                ],[
                    'name' => 'Comunas',
                    'url' => '/districts/admin-index',
                    'current' => ['action' => 'index','controller' => 'Districts','plugin' => false]
                ]
            ]
        ]
    ]
];